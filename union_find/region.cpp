#include "region.hpp"

Region::Region() : c(0), size(1), name(' '), parent(nullptr) {}
Region::Region(char c) : c(c), size(1), name(' '), parent(nullptr) {}

Region* Region::compress() {
    if (parent == nullptr) {
        return this;
    }
    else {
        Region* root = parent->compress();
        parent = root;
        return root;
    }
}
