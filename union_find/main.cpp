#include "ascii_image.hpp"
#include <iostream>
#include <string>
#include "region.hpp"

void show_usage() {
    std::cout << "Usage: AsciiArt [OPTION] TEXTFILE\n" <<
            "\nOPTION:\n" <<
            "  -h, --help\tshow this help message and exit\n" <<
            "  -l, --letter\tmark regions with letters instead of the original chars" <<
            "  -n INT\tminimum region size which is displayed (default: 20)" << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "Usage: AsciiArt [OPTION] TEXTFILE\n" <<
                "'AsciiArt --help' for more information." << std::endl;
        return 1;
    }

    // default parameters
    bool use_letters = false;
    int min_region_size = 20;

    for (int i = 1; i < argc; ++i) {
        if (argv[i] == std::string("--help") or argv[i] == std::string("-h")) {
            show_usage();
            return 0;
        }
        if (argv[i] == std::string("--letter") or argv[i] == std::string("-l")) {
            use_letters = true;
        }
        else if (argv[i] == std::string("-n")) {
            // TODO: catch exception
            min_region_size = std::stoi(argv[i+1]);
        }
    }

    std::string filename = argv[argc-1];
    AsciiImage img;
    if (not img.read(filename)) {
        std::cout << "Error: Couldn't read file " << filename << std::endl;
        return 42;
    }
    std::cout << "Das Kunstwerk im Original:" << std::endl;
    img.print();
    img.find_regions();
    img.mark_regions(min_region_size, use_letters);
    std::cout << "\nZusammenhängende Bereiche markiert:" << std::endl;
    img.print_regions();
    Region r = img.max_region();
    std::cout << "Die größte Region ist '" << r.name << "' mit " << r.size << " zusammenhängenden Feldern." << std::endl;
}
