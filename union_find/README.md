# Blatt 10, Aufgabe 1: Ein Bild sagt mehr als 1000 Zeichen (70%)

In der Bildverarbeitung versucht man oft zusammenhängende Regionen in Bildern zu finden, d. h. größerer Bereiche ähnlicher Farbe. Hierzu werden *Union-Find*-Strukturen benötigt.

1. Im Stud.IP ist ein ASCII-Bild in der Größe 80 × 29 Zeichen zu finden. Als erstes soll eine Methode geschrieben werden, die dieses Bild einliest und in der Konsole darstellt. Es ist zu beachten, dass beim Einlesen die Zeilenenden überlesen werden müssen.  
Was stellt das Bild dar?

2. Jedes Zeichen soll durch eine *Region* repräsentiert werden. Eine solche enthält das Zeichen selbst, einen Zähler für die Anzahl der Zeichen innerhalb der Region (anfangs 1), einen Bezeichner für die Region (wird später gesetzt) und eine Referenz auf die Elternregion, zu der diese Region gehört (zeigt anfangs auf sich selbst). Eine solche Region soll für jedes Zeichen in ein 80 × 29 Einträge großes Array eingetragen werden.

3. Man schreibe eine Methode *compress*, die zu einer Region die Wurzelregion heraussucht, indem sie sich so lange an den Elternreferenzen entlang hangelt, bis die Wurzel gefunden wurde. Die Wurzel erkennt man daran, dass ihr Elternzeiger auf sie selbst zeigt.

4. Nun ist eine Methode zu schreiben, die das Array aller Regionen durchläuft, und zwei Regionen vereinigt, wenn zwei im Bild benachbarte Zeichen gleich sind. Vereinigen heißt, dass eine der beiden Regionen die Elternregion der anderen wird, d. h. der Elternzeiger der einen Region auf die andere zeigt. Die Elternregion repräsentiert nun beide Teilregionen, weshalb auch die Anzahl der durch sie repräsentierten Zeichen entsprechend erhöht werden muss. Es ist zu beachten, dass nur Wurzelregionen vereinigt werden dürfen, d. h. man muss vor einer Vereinigung zu beiden Regionen die Wurzeln suchen. Des Weiteren soll beachtet werden, dass eine Region niemals mit sich selbst vereinigt wird.

5. Man weise nun allen Regionen mit mindestens 20 Zeichen einen Buchstaben als Bezeichnung zu und gebt das Bild mit diesen Bezeichnungen aus, indem an allen Stellen, die zu einer Region gehören, ihr Bezeichner ausgegeben wird. Regionen mit weniger als 20 Zeichen werden durch Leerzeichen dargestellt.  
Wie viele Zeichen enthält die größte Region?

6. Die Funktion *compress* ist so zu erweitern, dass sie eine echte Pfadkompression durchführt, also den durchlaufenen Pfad zur jeweiligen Wurzelregion verkürzt.
