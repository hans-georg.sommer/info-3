#include "ascii_image.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "region.hpp"

AsciiImage::AsciiImage() : width(0), height(0) {}

bool AsciiImage::read(std::string filename) {
    std::ifstream f(filename, std::ios::in);
    if (not f.is_open()) {
        return false;
    }
    bool need_resize = false;
    std::string line;
    int l;

    // NOTE: handles Windows ('\r\n') and Unix ('\n') line endings correctly,
    // doesn't care about old style MacOS ('\r')
    while (getline(f, line)) {
        if (line.back() == '\r') {
            line.pop_back();
        }
        if ((l = line.length()) > width) {
            if (width > 0) {
                need_resize = true;
            }
            width = l;
        }
        data.emplace_back(std::move(line));
    }
    height = data.size();

    if (need_resize) {
        for (std::string& s: data) {
            s.resize(width, ' ');
        }
    }
    return true;
}

void AsciiImage::combine_regions(int first, int second) {
    Region* root1 = regions[first].compress();
    Region* root2 = regions[second].compress();
    if (root1 != root2) {
        if (root1->size < root2->size) {
            std::swap(root1, root2);
        }
        root2->parent = root1;
        root1->size += root2->size;
    }
}

void AsciiImage::find_regions() {
    regions.reserve(width * height);
    int id;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            // init region
            regions.emplace_back(Region(data[y].at(x)));
            id = y * width + x;
            if (y > 0) {
                // check union to the top
                if (regions[id].c == regions[id-width].c) {
                    combine_regions(id, id - width);
                }
                // check union to the top left
                if (x > 0 && regions[id].c == regions[id-width-1].c) {
                    combine_regions(id, id - width - 1);
                }
                // check union to the top right
                if (x < width - 1 && regions[id].c == regions[id-width+1].c) {
                    combine_regions(id, id - width + 1);
                }
            }
            // check union to the left
            if (x > 0 && regions[id].c == regions[id-1].c) {
                combine_regions(id, id - 1);
            }
        }
    }
}

void AsciiImage::mark_regions(int min_size, bool use_letters) {
    // TODO: handling for > 26 regions (skip special chars)
    char identifier = 'A';
    for (Region &r : regions) {
        if (r.parent == nullptr && r.size >= min_size) {
            r.name = (use_letters) ? identifier : r.c;
            ++identifier;
        }
    }
}

const Region AsciiImage::max_region() const {
    Region max_r;
    int max_size = 0;
    for (Region const& r : regions) {
        if (r.size > max_size) {
            max_size = r.size;
            max_r = r;
        }
    }
    return max_r;
}

void AsciiImage::print_border() const {
    std::cout << "+" << std::string(width, '-') << "+" << std::endl;
}

void AsciiImage::print() const {
    print_border();
    for (std::string s : data) {
        std::cout << "|" << s << "|" << std::endl;
    }
    print_border();
}

void AsciiImage::print_regions() {
    int id;
    print_border();
    for (int y = 0; y < height; ++y) {
        std::cout << "|";
        for (int x = 0; x < width; ++x) {
            id = y * width + x;
            std::cout << regions[id].compress()->name;
        }
        std::cout << "|" << std::endl;
    }
    print_border();
}
