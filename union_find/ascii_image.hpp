#ifndef ASCII_IMAGE_HPP
#define ASCII_IMAGE_HPP

#include <string>
#include <vector>
#include "region.hpp"

class AsciiImage {
public:
    AsciiImage();
    bool read(std::string filename);
    void print() const;
    void find_regions();
    void mark_regions(int min_size, bool use_letters = false);
    void print_regions();
    const Region max_region() const; //const reference?

private:
    void combine_regions(int first, int second);
    void print_border() const;

    int width;
    int height;
    std::vector<std::string> data;
    std::vector<Region> regions;
};

#endif // ASCII_IMAGE_HPP
