#ifndef REGION_HPP
#define REGION_HPP

struct Region {
    Region();
    Region(char c);
    Region* compress();

    char c;
    int size;
    char name;
    Region* parent;
};

#endif // REGION_HPP
