public class SimpleKey implements Comparable<SimpleKey> {
    public int key, pos;

    public SimpleKey(int key, int pos) {
        this.key = key;
        this.pos = pos;
    }

    public String toString() {
        //~ return String.format("(k: %d, p: %d)", this.key, this.pos);
        return String.format("(%d, %d)", this.key, this.pos);
    }

    public int compareTo(SimpleKey other) {
        if (this.key < other.key) {
            return -1;
        }
        if (this.key > other.key) {
            return 1;
        }
        return 0;
    }
}
