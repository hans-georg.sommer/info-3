public class ExtendedKey implements Comparable<ExtendedKey> {
    public int key, pos;

    public ExtendedKey(int key, int pos) {
        this.key = key;
        this.pos = pos;
    }

    public String toString() {
        //~ return String.format("(k: %d, p: %d)", this.key, this.pos);
        return String.format("(%d, %d)", this.key, this.pos);
    }

    public int compareTo(ExtendedKey other) {
        if (this.key < other.key) {
            return -1;
        }
        if (this.key > other.key) {
            return 1;
        }
        // keys are equal, but pos has to be evaluated too
        if (this.pos < other.pos) {
            return -1;
        }
        if (this.pos > other.pos) {
            return 1;
        }
        return 0;
    }
}
