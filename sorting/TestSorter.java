import java.util.Arrays;

public class TestSorter {
    private static Integer[] A;
    private static Integer[] B;
    private static Integer[] C;
    private static String tpl = "%-20s%10s%10s%10s";

    private static void initArrays(int n) {
		A = new Integer[n];
		B = new Integer[n];
		C = new Integer[n];

        for (int i = 0; i < n; ++i) {
            A[i] = i + 1;
            B[i] = n - i;
            while(true) {
                // quick and dirty method for random array
                int r = (int)(Math.random() * n);
                if (C[r] == null) {
                    C[r] = i + 1;
                    break;
                }
            }
        }
    }


    private static void test_insertion_sort(Sorter s) {
        System.out.println(String.format(tpl, "Insertion sort",
                Sorter.insertion_sort(A.clone()),
                Sorter.insertion_sort(B.clone()),
                Sorter.insertion_sort(C.clone()) ));
    }


    private static void test_quicksort(Sorter s) {
        System.out.println(String.format(tpl, "Quicksort",
                Sorter.quicksort(A.clone()),
                Sorter.quicksort(B.clone()),
                Sorter.quicksort(C.clone()) ));
    }


    private static void test_quicksort_rand(Sorter s) {
        int runs = 7; // how many runs?
        int c[] = {0, 0, 0};
        int ca, cb, cc;
        for (int i = 0; i < runs; ++i) {
            c[0] += ca = Sorter.quicksort_rand(A.clone());
            c[1] += cb = Sorter.quicksort_rand(B.clone());
            c[2] += cc = Sorter.quicksort_rand(C.clone());
            //~ System.out.println(String.format(tpl, "Qs rand (run " + i + ")", ca, cb, cc));
        }
        System.out.println(String.format(tpl, "Qs rand (avg)", c[0]/runs, c[1]/runs, c[2]/runs));
    }


    private static void test_heapsort(Sorter s) {
        System.out.println(String.format(tpl, "Heapsort",
                Sorter.heapsort(A.clone()),
                Sorter.heapsort(B.clone()),
                Sorter.heapsort(C.clone()) ));
    }


    private static void usage() {
        System.out.println("Aufruf: java TestSorter INT");
        System.out.println("\nINT:\tLänge der zu sortierenden Liste");
        System.exit(1);
    }


	public static void main(String args[]) {
        int size = 10;

        SimpleKey[] simpleAsc = new SimpleKey[size];
        SimpleKey[] simpleDesc = new SimpleKey[size];
        SimpleKey[] simpleRand = new SimpleKey[size];
        ExtendedKey[] extendedAsc = new ExtendedKey[size];
        ExtendedKey[] extendedDesc = new ExtendedKey[size];
        ExtendedKey[] extendedRand = new ExtendedKey[size];

        for (int i = 0; i < size; ++i) {
            int half = (size < 2) ? 0 : i / (size / 2); // prevent division by zero
            simpleAsc[i] = new SimpleKey(half, i);
            simpleDesc[size-i-1] = new SimpleKey(half, i);
            extendedAsc[i] = new ExtendedKey(half, i);
            extendedDesc[size-i-1] = new ExtendedKey(half, i);
            while(true) {
                // same method as last time
                int r = (int)(Math.random() * size);
                if (simpleRand[r] == null) {
                    simpleRand[r] = new SimpleKey(half, i);
                    extendedRand[r] = new ExtendedKey(half, i);
                    break;
                }
            }
        }

        System.out.println("Ascending: " + Arrays.toString(simpleAsc));
        System.out.println("Descending: " + Arrays.toString(simpleDesc));
        System.out.println("Random: " + Arrays.toString(simpleRand));

        System.out.println("\n" + String.format(tpl, "Array", "Insertion", "Qicksort", "Heapsort"));

        System.out.println(String.format(tpl, "Simple ascending",
                Sorter.insertion_sort(simpleAsc.clone()),
                Sorter.quicksort(simpleAsc.clone()),
                Sorter.heapsort(simpleAsc.clone())));

        System.out.println(String.format(tpl, "Simple descending",
                Sorter.insertion_sort(simpleDesc.clone()),
                Sorter.quicksort(simpleDesc.clone()),
                Sorter.heapsort(simpleDesc.clone())));

        System.out.println(String.format(tpl, "Simple random",
                Sorter.insertion_sort(simpleRand.clone()),
                Sorter.quicksort(simpleRand.clone()),
                Sorter.heapsort(simpleRand.clone())));

        System.out.println(String.format(tpl, "Extended ascending",
                Sorter.insertion_sort(extendedAsc.clone()),
                Sorter.quicksort(extendedAsc.clone()),
                Sorter.heapsort(extendedAsc.clone())));

        System.out.println(String.format(tpl, "Extended descending",
                Sorter.insertion_sort(extendedDesc.clone()),
                Sorter.quicksort(extendedDesc.clone()),
                Sorter.heapsort(extendedDesc.clone())));

        System.out.println(String.format(tpl, "Extended random",
                Sorter.insertion_sort(extendedRand.clone()),
                Sorter.quicksort(extendedRand.clone()),
                Sorter.heapsort(extendedRand.clone())));

        //~ int n = 0;
        //~ if (args.length != 1) {
            //~ usage();
        //~ }
        //~ try {
            //~ n = Integer.parseInt(args[0]);
        //~ }
        //~ catch (NumberFormatException e) {
            //~ usage();
        //~ }

        //~ initArrays(n);

        //~ System.out.println("\n" + String.format(tpl, "Algorithm", "{1,...,n}", "{n,...,1}", "{random}") + "\n");

        //~ test_insertion_sort(s);
        //~ test_quicksort(s);
        //~ test_quicksort_rand(s);
        //~ test_heapsort(s);
	}
}
