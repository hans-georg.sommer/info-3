import java.util.Arrays;

public class Sorter {
    private static boolean pivot_rand = false; // switch between pivot selection strategies


    public static <T extends Comparable<T>> int insertion_sort(T[] A) {
        int keyComp = 0;
        int n = A.length;

        for (int j = 1; j < n; ++j) {
            int i = j;
            T key = A[j];
            ++keyComp; // since i is always > 0, at least one comparison is neccessary
            while (i > 0 && A[i-1].compareTo(key) > 0) {
                ++keyComp;
                // move every element > key by one position
                A[i] = A[i-1];
                --i;
            }
            // insert current element into the sorted sequence
            A[i] = key;
        }

        return keyComp;
    }


    private static <T extends Comparable<T>> int quicksort_internal(T[] A, int left, int right) {
        int pivot;
        int left_org = left;
        int right_org = right;
        int keyComp = 0;
        if (pivot_rand) {
            // random pivot element
            pivot = left + (int)(Math.random() * (right - left));
        }
        else {
            // take pivot element from the middle of the list
            pivot = (left + right) / 2;
        }
        while (true) {
            // search element with lowest index and value >= value of pivot
            while (left < pivot) {
                ++keyComp;
                if (A[pivot].compareTo(A[left]) > 0) {
                    ++left;
                }
                else {
                    break;
                }
            }
            // search element with highest index and value <= value of pivot
            while (right > pivot) {
                ++keyComp;
                if (A[pivot].compareTo(A[right]) < 0) {
                    --right;
                }
                else {
                    break;
                }
            }
            if (left != right) {
                // swap elements and adjust search interval
                T t = A[left];
                A[left] = A[right];
                A[right] = t;
                if (pivot == right) {
                    pivot = left;
                    --right;
                }
                else if (pivot == left) {
                    pivot = right;
                    ++left;
                }
                else {
                    ++left;
                    --right;
                }
            }
            else {
                // left == right, everything done for this round
                break;
            }
        }

        // sort left and right part recursively (excluding pivot)
        if (pivot - left_org > 1) {
            keyComp += quicksort_internal(A, left_org, pivot - 1);
        }
        if (right_org - pivot > 1) {
            keyComp += quicksort_internal(A, pivot + 1, right_org);
        }
        return keyComp;
    }


    public static <T extends Comparable<T>> int quicksort(T[] A) {
        pivot_rand = false;
        int keyComp = quicksort_internal(A, 0, A.length - 1);
        return keyComp;
    }


    public static <T extends Comparable<T>> int quicksort_rand(T[] A) {
        pivot_rand = true;
        int keyComp = quicksort_internal(A, 0, A.length - 1);
        return keyComp;
    }


    public static <T extends Comparable<T>> int reheap(T[] A, int first, int last) {
        int keyComp = 0;
        int maxChild;
        int leftChild = 2 * first + 1;
        int rightChild = leftChild + 1;

        if (leftChild > last) {
            // element has no child element
            return 0;
        }
        if (leftChild == last) {
            // only one child element
            maxChild = last;
        }
        else {
            // determine max child element
            ++keyComp;
            if (A[leftChild].compareTo(A[rightChild]) < 0) {
                maxChild = rightChild;
            }
            else {
                maxChild = leftChild;
            }
        }
        ++keyComp;
        // if child > parent, switch them...
        if (A[first].compareTo(A[maxChild]) < 0) {
            T t = A[first];
            A[first] = A[maxChild];
            A[maxChild] = t;
            // ...and continue recursively
            keyComp += reheap(A, maxChild, last);
        }
        return keyComp;
    }


    public static <T extends Comparable<T>> int heapsort(T[] A) {
        int keyComp = 0;
        int n = A.length - 1;
        if (n < 1) {
            return 0;
        }

        // buildheap()
        // note: n is already reduced by 1
        for (int j = n/2; j >= 0; --j) {
            keyComp += reheap(A, j, n);
        }

        // move biggest element to final position and reduce heap size
        while (n > 0) {
            T t = A[0];
            A[0] = A[n];
            A[n] = t;
            --n;
            // rebuild the heap structure
            keyComp += reheap(A, 0, n);
        }
        //~ System.out.println(Arrays.toString(A));
        return keyComp;
    }
}
