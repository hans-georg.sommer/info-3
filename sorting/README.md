# Blatt 5, Aufgabe 3: Jede Menge Sortierverfahren (50%)
1. Man erstelle die Klasse `Sorter`, welche Methoden für *Insertion-Sort*, *Quicksort* mit dem mittleren Element als Pivot sowie für *randomisiertes Quicksort* zur Verfügung stellen soll. Jeder dieser Methoden soll ein zu sortierendes Feld übergeben werden können. Der Rückgabe-Wert der jeweiligen Methode soll die Anzahl der für den Sortiervorgang benötigten Schlüsselvergleiche sein.  
Um dies zu ermöglichen, sollen die zu sortierenden Klassen eine Methode implementieren, die einen Vergleich zweier Instanzen des Typs erlaubt. In Java (und auch anderen Sprachen) existiert dazu in der Klassenbibliothek die Schnittstelle `Comparable`. (Java Klassen wie z. B. `Integer`, `String` oder `Double` implementieren diese Schnittstelle bereits.) Die Elemente der Felder, welche den Methoden der Klasse `Sorter` übergeben werden können, müssen miteinander vergleichbar sein.

2. Man schreibe eine Testklasse, welche die Anzahl der Schlüsselvergleiche bei den drei Sortierverfahren
    - für zufällig erzeugte Permutationen der Menge {1,…,*n*}
    - für die Folgen (1,…,*n*), (*n*,…,1)  
für vorzugebendes *n* ermittelt.  
Für den Test des randomisierten Quicksort sollte für jede Eingabefolge das Verfahren *mehrmals* angewendet werden!

# Blatt 6, Aufgabe 3: Erweiterung der Sortierverfahren (40%)

1. Man ergänze die Klasse Sorter aus Aufgabe 3 vom Blatt 5 um die Methode `heapsort(Comparable[] A)`, die ihrerseits die ebenfalls zu implementierenden Methode `reheap(Comparable[] A, int from, int to)` benutzt. Wie bei den anderen Sortierverfahren sind auch hier die benötigten Schlüsselvergleiche zurückzugeben.

2. Man erstelle die zwei Klassen `SimpleKey` und `ExtendedKey`, welche die Schnittstelle `Comparable` implementieren. Sie sollen zwei Eigenschaften `key` und `pos` vom Typ *Integer* haben. `SimpleKeys` sollen nur den Schlüssel vergleichen, `ExtendedKey`s auch das Feld `pos`, allerdings mit niedrigerer Priorität.

3. Man erzeuge nun zwei Felder mit je 10 Schlüsseln, eins mit `SimpleKey`- und eins mit `ExtendedKey`-Instanzen. Dabei soll das Datenfeld `pos` mit dem Index, den das Objekt im Feld hat, initialisiert werden. Das Schlüsselfeld soll in beiden Feldern jeweils fünfmal mit den Werten 0 und 1 initialisiert werden.

4. Man wende Insertion-Sort, Quicksort (eine beliebige Variante) und Heapsort zunächst auf die `SimpleKeys`, dann auf die `ExtendedKeys` an. Was fällt auf? Ändert sich bei den `ExtendedKeys` die Zahl der benötigten Schlüsselvergleiche?

## Hinweise:
- Diese Aufgabe ist eine Programmieraufgabe. Um die volle Punktzahl zu erhalten, muss eine lauffähige sowie ausreichend dokumentierte Implementation in Java erstellt und vom jeweiligen Tutor testiert werden. Näheres dazu in den Übungen.
- Der Code der Implementierung sowie eine Testausgabe sind der schriftlichen Abgabe beizufügen.
